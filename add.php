<html>
<head>
    <meta charset="utf8"/>
    <title><?= $_GET['view'] == "add" ? 'Uus postitus' : 'Muuda postitust'; ?></title>
    <link rel="stylesheet" type="text/css" href="assets/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="assets/styles.css">
</head>
<body>
<div class="container">
    <div id="content">
        <div class="col-md-6 col-xs-5">
            <?php foreach (message_list() as $message): ?>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?= $message; ?>
                </div>
            <?php endforeach; ?>
            <h1><?= $_GET['view'] == "add" ? 'Uus postitus' : 'Muuda postitust'; ?></h1>
            <div class="panel panel-default main">
                <div class="panel-body">
                    <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>" class="form-horizontal">
                        <input type="hidden" name="action" value="<?= $_GET['view'] == "add" ? 'add' : 'edit'; ?>">
                        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                        <input type="hidden" name="id"
                               value="<?= isset($_GET['id']) ? $_POST['id'] = $_GET['id'] : ''; ?>">
                        <div class="form-group required">
                            <label for="title" class="col col-md-3 control-label">Pealkiri</label>
                            <div class="col col-md-9 required">
                                <input name="title" class="form-control" maxlength="90" type="text" id="title" required
                                       value="<?= isset($_POST['title']) ? htmlentities($_POST['title']) : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group required">
                            <label for="content" class="col col-md-3 control-label">Sisu</label>
                            <div class="col col-md-9 required">
                                <textarea name="content" class="form-control" cols="30" rows="6" id="content"
                                          required><?= isset($_POST['content']) ? htmlentities($_POST['content']) : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comment" class="col col-md-3 control-label">Kommentaar</label>
                            <div class="col col-md-9 required">
                                <textarea name="comment" class="form-control" cols="30" rows="6" maxlength="150"
                                          id="comment"><?= isset($_POST['comment']) ? htmlentities($_POST['comment']) : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="link" class="col col-md-3 control-label">Link</label>
                            <div class="col col-md-9 required">
                                <input name="link" class="form-control" maxlength="200" type="text" id="link"
                                       value="<?= isset($_POST['link']) ? htmlentities($_POST['link']) : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group required">
                            <label for="category" class="col col-md-3 control-label">Kategooria</label>
                            <div class="col col-md-9 required">
                                <select name="category" class="form-control" id="category" required="required">
                                    <option value>Vali kategooria</option>
                                    <?php foreach (getCategories() as $cat) {
                                        $selected = '';
                                        if (isset($_POST['category']) && $_POST['category'] == $cat['id']) {
                                            $selected = "selected='selected'";
                                        }
                                        echo "<option value='{$cat['id']}' $selected >{$cat['category']}</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div id="buttons">
                            <a href="index.php" class="btn btn-danger">Loobu</a>
                            <input class="btn btn-success" type="submit"
                                   value="<?= $_GET['view'] == "add" ? 'Lisa postitus' : 'Muuda postitust'; ?>">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/scripts.js"></script>
</body>
</html>