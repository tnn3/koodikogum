<html>
<head>
    <meta charset="utf8"/>
    <title>Logi sisse</title>
    <link rel="stylesheet" type="text/css" href="assets/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="assets/styles.css">
</head>
<body>
<div class="container">
    <div id="content">
        <?php foreach (message_list() as $message): ?>
            <div class="center col-md-5 alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <div class="col-md-4 center">
            <form class="form-signin" method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                <h2 class="form-signin-heading">Logi sisse</h2>
                <input type="hidden" name="action" value="login">
                <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                <label for="username" class="sr-only">Kasutajanimi</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="Kasutajanimi" required autofocus>
                <label for="password" class="sr-only">Parool</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Parool" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Logi sisse</button>
                või
                <a href="<?= $_SERVER['PHP_SELF']; ?>?view=register">registreeri konto</a>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/bootstrap.min.js"></script>
</body>
</html>
