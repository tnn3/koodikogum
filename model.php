<?php
$config = parse_ini_file('config.ini');

$l = mysqli_connect($config['host'], $config['username'], $config['password'], $config['dbname']);
mysqli_query($l, 'SET CHARACTER SET UTF8');

function getPosts($page)
{
    global $l;
    $max = 5;
    $start = ($page - 1) * $max;

    $query = 'SELECT posts.id, title, content, link, comment, category, username FROM posts 
    JOIN categorys ON categorys.id = posts.categorys_id 
    JOIN users ON users.id = posts.users_id';
    if (!empty(searchFilters())) {
        $query .= searchFilters();
    }
    $query .= ' ORDER BY id DESC LIMIT ?,?';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_bind_param($stmt, 'ii', $start, $max);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $title, $content, $link, $comment, $category, $user);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'id' => $id,
            'title' => $title,
            'content' => $content,
            'link' => $link,
            'comment' => $comment,
            'category' => $category,
            'user' => $user,
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

function getPost($id)
{
    global $l;

    $query = 'SELECT id, title, content, link, comment, categorys_id, users_id FROM posts WHERE id=?';

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $title, $content, $link, $comment, $category, $user);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'id' => $id,
            'title' => $title,
            'content' => $content,
            'link' => $link,
            'comment' => $comment,
            'category' => $category,
            'user' => $user,
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

function addPost($title, $content, $link, $comment, $category, $user)
{
    global $l;

    $query = 'INSERT INTO posts (title, content, link, comment, categorys_id, users_id) VALUES (?, ?, ?, ?, ?, ?)';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 'ssssii', $title, $content, $link, $comment, $category, $user);
    mysqli_stmt_execute($stmt);

    $id = mysqli_stmt_insert_id($stmt);

    mysqli_stmt_close($stmt);

    return $id;
}

function deletePost($id)
{
    global $l;

    $query = 'DELETE FROM posts WHERE id=? LIMIT 1';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);

    $deleted = mysqli_stmt_affected_rows($stmt);

    mysqli_stmt_close($stmt);

    return $deleted;
}

function editPost($id, $title, $content, $link, $comment, $category)
{
    global $l;

    $query = 'UPDATE posts SET title=?, content=?, link=?, comment=?, categorys_id=? WHERE id=?';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_bind_param($stmt, 'ssssii', $title, $content, $link, $comment, $category, $id);
    mysqli_stmt_execute($stmt);
    if (mysqli_stmt_error($stmt)) {
        return false;
    }
    mysqli_stmt_close($stmt);

    return true;
}

function addUser($username, $password)
{
    global $l;

    $hash = password_hash($password, PASSWORD_DEFAULT);

    $query = 'INSERT INTO users (username, password) VALUES (?, ?)';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_bind_param($stmt, 'ss', $username, $hash);
    mysqli_stmt_execute($stmt);

    $id = mysqli_stmt_insert_id($stmt);

    mysqli_stmt_close($stmt);

    return $id;
}

function getUser($username, $password)
{
    global $l;

    $query = 'SELECT id, password FROM users WHERE username=?';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 's', $username);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $hash);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);
    if (password_verify($password, $hash)) {
        return $id;
    }

    return false;
}

function getCategories()
{
    global $l;

    $query = 'SELECT id, category FROM categorys';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $category);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'id' => $id,
            'category' => $category,
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

function searchFilters()
{
    if (isset($_GET['title']) || isset($_GET['content']) || isset($_GET['category'])) {

        $where = " WHERE TRUE";
        if (array_key_exists('title', $_GET) && !empty($_GET['title'])) {
            $getTitle = $_GET['title'];
            $where .= ' AND posts.title LIKE "%' . $getTitle . '%"';
        }
        if (array_key_exists('content', $_GET) && !empty($_GET['content'])) {
            $getContent = $_GET['content'];
            $where .= ' AND posts.content LIKE "%' . $getContent . '%"';
        }
        if (array_key_exists('category', $_GET) && filter_var($_GET['category'], FILTER_VALIDATE_INT)) {
            $getCategory = $_GET['category'];
            $where .= " AND posts.categorys_id = $getCategory";
        }
        return $where;
    }
    return '';
}