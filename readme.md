# Koodikogum on väikeste koodi jupide hoidla. #
Koodijupid on omavahel jagatud keelte järgi kategooriatesse.

Igal koodijupil on:

    koodi postitanud kasutaja,
    teda iseloomustav pealkiri,
    sisu,
    kategooria,
    kommentaar koodi kohta (valikuline),
    link rohkema info saamiseks (valikuline)

Koodikogumis on võimalus:

    Registreerida oma kasutaja ja sisse logida,
    lisada uut koodi:
        sisu lisamises saab tab klahvi kasutada,
        valideerimine
    muuta olemas olevat,
    kustutada postitatut,
    avada/sulgeda koodi juppe pealkirjal vajutades,
    otsida koodi otsinguga sisestatud koodide hulgast,
    lehitseda tulemusi

Andmebaasi andmed on salvestatud eraldi faili, millele on ligipääs väliselt keelatud.

Kasutatud väline kood:
    Jquery
    Bootstrap CSS+JS
    Praktikumide kood