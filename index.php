<?php
session_start();

if (empty($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = bin2hex(openssl_random_pseudo_bytes(20));
}

require 'model.php';
require 'controller.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = false;

    if (!empty($_POST['csrf_token']) && $_POST['csrf_token'] == $_SESSION['csrf_token']) {
        switch ($_POST['action']) {
            case 'add':
                $title = $_POST['title'];
                $content = $_POST['content'];
                $link = $_POST['link'];
                $comment = $_POST['comment'];
                $category = intval($_POST['category']);
                $user = $_SESSION['login'];
                $result = controller_add($title, $content, $link, $comment, $category, $user);
                break;
            case 'delete':
                $id = intval($_POST['id']);
                $result = controller_delete($id);
                break;
            case 'edit':
                $id = intval($_POST['id']);
                $title = $_POST['title'];
                $content = $_POST['content'];
                $link = $_POST['link'];
                $comment = $_POST['comment'];
                $category = intval($_POST['category']);
                $result = controller_edit($id, $title, $content, $link, $comment, $category);
                break;
            case 'register':
                $username = $_POST['username'];
                $password = $_POST['password'];
                $result = controller_register($username, $password);
                break;
            case 'login':
                $username = $_POST['username'];
                $password = $_POST['password'];
                $result = controller_login($username, $password);
                break;
            case 'logout':
                $result = controller_logout();
                break;
        }
    } else {
        message_add('Vigane päring, CSRF token ei vasta oodatule');
    }

    header('Location: ' . $_SERVER['PHP_SELF']);
    exit;
}

if (!empty($_GET['view'])) {
    switch ($_GET['view']) {
        case 'login':
            require 'login.php';
            break;
        case 'register':
            require 'register.php';
            break;
        case 'add':
            require 'add.php';
            break;
        case 'edit':
            setPostValues();
            require 'add.php';
            break;
        default:
            header('Content-type: text/plain; charset=utf-8');
            echo 'Lehte ei leitud';
            exit;
    }
} else {
    if (!controller_user()) {
        header('Location: ' . $_SERVER['PHP_SELF'] . '?view=login');
        exit;
    }

    if (empty($_GET['page'])) {
        $page = 1;
    } else {
        $page = intval($_GET['page']);
        if ($page < 1) {
            $page = 1;
        }
    }

    require 'view.php';
}