-- -----------------------------------------------------
-- Schema koodikogum
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `koodikogum` DEFAULT CHARACTER SET utf8 COLLATE utf8_estonian_ci ;
USE `koodikogum` ;

-- -----------------------------------------------------
-- Table `koodikogum`.`categorys`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koodikogum`.`categorys` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `koodikogum`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koodikogum`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `koodikogum`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `koodikogum`.`posts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(90) NOT NULL,
  `content` TEXT NOT NULL,
  `link` VARCHAR(200) NULL,
  `comment` VARCHAR(150) NULL,
  `categorys_id` INT UNSIGNED NOT NULL,
  `users_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_posts_categorys_idx` (`categorys_id` ASC),
  INDEX `fk_posts_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_posts_categorys`
    FOREIGN KEY (`categorys_id`)
    REFERENCES `koodikogum`.`categorys` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_posts_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `koodikogum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO categorys(category) values ("PHP"),("CSS"),("Java"),("Python"),("HTML");