<?php

function controller_add($title, $content, $link, $comment, $category, $user)
{
    if (!controller_user()) {
        message_add('Tegevus eeldab sisselogimist');
        return false;
    }

    if (empty($title) || empty($content) || empty($category ||
            mb_strlen($_POST ['title'], "UTF-8") > 90) ||
        mb_strlen($_POST ['content'], "UTF-8") > 450
    ) {
        message_add('Vigased sisendandmed');
        return false;
    }
    if (addPost($title, $content, $link, $comment, $category, $user)) {
        message_add('Postitus lisati');
        return true;
    }
    message_add('Postituse lisamine ebaõnnestus');
    return false;
}

function controller_delete($id)
{
    if (!controller_user()) {
        message_add('Tegevus eeldab sisselogimist');
        return false;
    }
    if ($id <= 0) {
        message_add('Vigased sisendandmed');
        return false;
    }
    if (deletePost($id)) {
        message_add('Postitus kustutati');
        return true;
    }
    message_add('Rea kustutamine ebaõnnestus');
    return false;
}

function controller_edit($id, $title, $content, $link, $comment, $category)
{
    if (!controller_user()) {
        message_add('Tegevus eeldab sisselogimist');
        return false;
    }
    if (empty($title) || empty($content) || empty($category ||
            mb_strlen($_POST ['title'], "UTF-8") > 90) ||
        mb_strlen($_POST ['content'], "UTF-8") > 450
    ) {
        message_add('Vigased sisendandmed');
        return false;
    }
    if (editPost($id, $title, $content, $link, $comment, $category)) {
        message_add('Postitust muudeti');
        return true;
    }
    message_add('Postituse muutmine ebaõnnestus');
    return false;
}

function controller_user()
{
    if (empty($_SESSION['login'])) {
        return false;
    }
    return $_SESSION['login'];
}

function controller_register($username, $password)
{
    if ($username == '' || $password == '') {
        message_add('Vigased sisendandmed');
        return false;
    }
    if (addUser($username, $password)) {
        message_add('Konto on registreeritud');
        return true;
    }
    message_add('Konto registreerimine ebaõnnestus, kasutajanimi võib olla juba võetud');
    return false;
}

function controller_login($username, $password)
{
    if ($username == '' || $password == '') {
        message_add('Vigased sisendandmed');
        return false;
    }
    $id = getUser($username, $password);
    if (!$id) {
        message_add('Vigane kasutajanimi või parool');
        return false;
    }
    session_regenerate_id();
    $_SESSION['login'] = $id;
    return $id;
}

function controller_logout()
{

    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time() - 42000, '/');
    }

    $_SESSION = array();

    session_destroy();
    return true;
}

function message_add($message)
{
    if (empty($_SESSION['messages'])) {
        $_SESSION['messages'] = array();
    }
    $_SESSION['messages'][] = $message;
}

function message_list()
{
    if (empty($_SESSION['messages'])) {
        return array();
    }
    $messages = $_SESSION['messages'];
    $_SESSION['messages'] = array();
    return $messages;
}

function setPostValues()
{
    if (!empty($_GET['id'])) {
        if ($rows = getPost($_GET['id'])) {
            $_POST['id'] = $rows[0]['id'];
            $_POST['title'] = $rows[0]['title'];
            $_POST['content'] = $rows[0]['content'];
            $_POST['link'] = $rows[0]['link'];
            $_POST['comment'] = $rows[0]['comment'];
            $_POST['category'] = $rows[0]['category'];
            $_POST['user'] = $rows[0]['user'];
        } else {
            message_add("Postitust ei leitud");
        }
    } else
        message_add("Postitust ei leitud");
}