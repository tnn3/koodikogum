<!doctype HTML>
<html>

<head>
    <title>Koodikogum</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="assets/styles.css">
</head>

<body>

<div class="container">
    <div>
        <?php foreach (message_list() as $message): ?>
            <div class="col-md-10 alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <h1>Koodikogum</h1>
        <div class="col-md-7 col-xs-8 main">
            <?php
            foreach (getPosts($page) as $line): ?>
                <a href="#" class="toggler"><?= htmlentities($line['title']) . ' - ' . $line['category']; ?></a>
                <br/>
                <div class="toggle">
                    <pre><code><?= htmlentities($line['content']); ?></code></pre>
                    <p>Lisas: <?= htmlentities($line['user']); ?></p>
                    <?php if (!empty($line['comment'])) { ?>
                        <p>Kommentaar: <?= htmlentities($line['comment']); ?></p>
                    <?php } ?>
                    <?php if (!empty($line['link'])) { ?>
                        <p>Link: <a href="<?= htmlentities($line['link']); ?>"><?= $line['link']; ?></a></p>
                    <?php } ?>
                    <div class="buttons">
                        <a href="<?= $_SERVER['PHP_SELF']; ?>?view=edit&id=<?= $line['id'] ?>" class="btn btn-primary">Muuda</a>
                        <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                            <input type="hidden" name="action" value="delete">
                            <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                            <input type="hidden" name="id" value="<?= $line['id'] ?>">
                            <button type="submit" class="btn btn-danger"
                                    onclick="return confirm('Kas olete kindel, et soovite kustutada postituse?')">
                                Kustuta
                            </button>
                        </form>
                    </div>
                </div>
            <?php endforeach; ?>
            <ul class="pagination">
                <li><a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page - 1; ?>"><<</a></li>
                <li class="active"><a href="#"><?= $page; ?></a></li>
                <li><a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page + 1; ?>">>></a></li>
            </ul>
        </div>
        <div class="col-md-3 col-xs-4 main">
            <div class="panel panel-default">
                <div class="panel-heading">Tegevused</div>
                <div class="panel-body">
                    <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                        <input type="hidden" name="action" value="logout">
                        <input type="hidden" name="csrf_token" value="<?= $_SESSION['csrf_token']; ?>">
                        <button class="btn btn-default" type="submit">Logi välja</button>
                    </form>
                    <a href="<?= $_SERVER['PHP_SELF']; ?>?view=add" class="btn btn-default">Lisa uus kood</a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Otsing</div>
                <div class="panel-body">
                    <div class="filters">
                        <form action="<?= $_SERVER['PHP_SELF']; ?>" class="filter" method="get">
                            <div class="form-group">
                                <label for="title">Pealkiri</label>
                                <div class="input text">
                                    <input class="form-control" name="title" id="title" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="content">Sisu</label>
                                <div class="input text">
                                    <input class="form-control" id="content" name="content" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category">Kategooria</label>
                                <div class="input">
                                    <select class="form-control" name="category" id="category">
                                        <option value>Vali kategooria</option>
                                        <?php foreach (getCategories() as $cat): ?>
                                            <option value="<?= $cat['id']; ?>"><?= $cat['category']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <a href="<?= $_SERVER['PHP_SELF']; ?>" class="btn btn-default">Puhasta väljad</a>
                            <div class="submit">
                                <input class="btn btn-success" type="submit" value="Otsi">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/scripts.js"></script>
<script type="text/javascript" src="assets/bootstrap.min.js"></script>
</body>
</html>